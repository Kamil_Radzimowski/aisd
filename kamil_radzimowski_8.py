def lcsLength_1(x, y):
    m = len(x)
    n = len(y)
    c = [[0 for _ in range(n + 1)]
         for _ in range(m + 1)]
    b = [[0 for _ in range(n + 1)]
         for _ in range(m + 1)]
    for i in range(1, m + 1):
        for j in range(1, n + 1):
            if x[i - 1] == y[j - 1]:
                c[i][j] = c[i - 1][j - 1] + 1
                b[i][j] = '\\'
            else:
                if c[i - 1][j] >= c[i][j - 1]:
                    c[i][j] = c[i - 1][j]
                    b[i][j] = "|"
                else:
                    c[i][j] = c[i][j - 1]
                    b[i][j] = "-"
    return c, b


def lcsLength_2(x, y):
    m = len(x)
    n = len(y)
    c = [[0 for _ in range(n + 1)]
         for _ in range(m + 1)]
    b = [[0 for _ in range(n + 1)]
         for _ in range(m + 1)]
    for i in range(1, m + 1):
        for j in range(1, n + 1):
            if x[i - 1] == y[j - 1]:
                c[i][j] = c[i - 1][j - 1] + 1
                b[i][j] = '\\'
            else:
                if c[i - 1][j] > c[i][j - 1]:
                    c[i][j] = c[i - 1][j]
                    b[i][j] = "|"
                else:
                    c[i][j] = c[i][j - 1]
                    b[i][j] = "-"
    return c, b


def PrintLCS(x, b, i, j):
    if i == 0 or j == 0:
        return
    if b[i][j] == "\\":
        PrintLCS(x, b, i - 1, j - 1)
        print(x[i - 1])
    elif b[i][j] == "|":
        PrintLCS(x, b, i - 1, j)
    else:
        PrintLCS(x, b, i, j - 1)


def helper(x, b, i, j, acc):
    if i == 0 or j == 0:
        return acc
    if b[i][j] == "\\":
        acc += x[i - 1]
        return helper(x, b, i - 1, j - 1, acc)
    elif b[i][j] == "|":
        return helper(x, b, i - 1, j, acc)
    else:
        return helper(x, b, i, j - 1, acc)


def print_all_lcs(x, b, c):
    result = []
    max_len = 0
    for row in c:
        for entry in row:
            if entry > max_len:
                max_len = entry

    for i in range(len(b)):
        for j in range(len(b[i])):
            if c[i][j] == max_len:
                result.append(helper(x, b, i, j, "")[::-1])
    return set(result)


STR_1 = "aabbcc"
STR_2 = "abacb"
(c, b) = lcsLength_1(STR_1, STR_2)
(c_2, b_2) = lcsLength_2(STR_1, STR_2)
# PrintLCS(STR_1, b, len(STR_1), len(STR_2))
print(print_all_lcs(STR_1, b, c))
print(print_all_lcs(STR_1, b_2, c_2))
