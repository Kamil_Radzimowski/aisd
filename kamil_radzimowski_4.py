class Node:
    def __init__(self, x):
        self.key = x
        self.next = None
        self.prev = None


class LinkedList:
    def __init__(self):
        self.head = Node(None)
        self.head.next = self.head
        self.head.prev = self.head

    def listInsert(self, node):
        previous_first = self.head.next
        new_node = Node(node.key)
        if previous_first.key is None:
            self.head.next = new_node
            self.head.prev = new_node
            new_node.prev = self.head
            new_node.next = self.head
        else:
            temp = self.head.next
            self.head.next = new_node
            new_node.next = temp
            new_node.prev = self.head
            temp.prev = new_node

    def listPrint(self):
        counter = 0
        x = self.head.next
        print("[none] <->", end="")
        while x.key is not None:
            counter += 1
            print(f" [{x.key}] <->", end="")
            x = x.next
        print("")

    def listSearch(self, k):
        x = self.head.next
        while x.key is not None and x.key != k:
            x = x.next
        return x

    def listDelete(self, x):
        x.prev.next = x.next
        x.next.prev = x.prev


def mergeLists(l1, list2):
    result = LinkedList()
    if l1.head.next.key is None:
        return list2
    if list2.head.next.key is None:
        return l1

    x = list2.head.prev
    result.listInsert(x)
    while x.prev.key is not None:
        x = x.prev
        result.listInsert(x)

    y = l1.head.prev
    result.listInsert(y)
    while y.prev.key is not None:
        y = y.prev
        result.listInsert(y)

    return result


def removeDuplicates(l):
    result = LinkedList()
    seen = []
    x = l.head.prev
    result.listInsert(x)
    seen.append(x.key)
    while x.prev.key is not None and x.prev.key not in seen:
        x = x.prev
        seen.append(x.key)
        result.listInsert(x)
    return result


l = LinkedList()
l.listInsert(Node(1))
l.listInsert(Node(2))
l.listInsert(Node(5))
l.listInsert(Node(5))
print("list 1: ", end="")
l.listPrint()

print(f"To delete: {l.listSearch(2).key} ", end="")
temp = l.listSearch(2)

l.listDelete(temp)
print(f"After deletion: ", end="")
l.listPrint()

l2 = LinkedList()
l2.listInsert(Node(3))
l2.listInsert(Node(4))

print("Second list: ", end="")
l2.listPrint()

print("Merge: ", end="")
l3 = mergeLists(l, l2)
print("l1: ", end="")
l.listPrint()
print("l2: ", end="")
l2.listPrint()
print("l3: ", end="")
l3.listPrint()

print("Removal of duplicates")

l4 = removeDuplicates(l)
print("l1: ", end="")
l.listPrint()
print("No duplicates: ", end="")
l4.listPrint()
