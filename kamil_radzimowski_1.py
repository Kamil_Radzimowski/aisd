import math
from timeit import default_timer as timer
import pandas as pd
import matplotlib.pyplot as plt

f1_data = []
f2_data = []
f3_data = []
f4_data = []
f5_data = []


def f1(n):
    s = 0
    for j in range(1, n):
        s = s + 1 / j
    return s


def f2(n):
    s = 0
    for j in range(1, n):
        for k in range(1, n):
            s = s + k / j
    return s


def f3(n):
    s = 0
    for j in range(1, n):
        for k in range(j, n):
            s = s + k / j
    return s


def f4(n):
    s = 0
    for j in range(1, n):
        k = 2
        while k <= n:
            s = s + k / j
            k = k * 2
    return s


def f5(n):
    s = 0
    k = 2
    while k <= n:
        s = s + 1 / k
        k = k * 2
    return s


nn = [2000, 4000, 8000, 16000, 32000]

# F1
print("F1")
for n in nn:
    start = timer()
    f1(n)
    stop = timer()
    Tn = stop - start
    f1_data.append([Tn])
    Fn = n
    print(n, Tn, Fn / Tn)

# inne funkcje czasu:

# Fn=math.log(n,2)
# Fn=n
# Fn=100*n
# Fn=n*math.log(n,2)
# Fn=n*n

# F2
print("F2")
for n in nn:
    start = timer()
    f2(n)
    stop = timer()
    Tn = stop - start
    Fn = n * n
    f2_data.append([Tn])
    print(n, Tn, Fn / Tn)

print("F3")
for n in nn:
    start = timer()
    f3(n)
    stop = timer()
    Tn = stop - start
    Fn = n * n
    f3_data.append([Tn])
    print(n, Tn, Fn / Tn)

print("F4")
for n in nn:
    start = timer()
    f4(n)
    stop = timer()
    Tn = stop - start
    Fn = n * math.log(n, 2)
    f4_data.append([Tn])
    print(n, Tn, Fn / Tn)

print("F5")
for n in nn:
    start = timer()
    f5(n)
    stop = timer()
    Tn = stop - start
    Fn = math.log(n, 2)
    f5_data.append([Tn])
    print(n, Tn, Fn / Tn)

fig, ax = plt.subplots()
fig2, f2_plot = plt.subplots()
fig3, f3_plot = plt.subplots()
fig4, f4_plot = plt.subplots()
fig5, f5_plot = plt.subplots()

f2_plot.plot(nn, f2_data, linewidth=2.0)
ax.plot(nn, f1_data, linewidth=2.0)
f3_plot.plot(nn, f3_data, linewidth=2.0)
f4_plot.plot(nn, f4_data, linewidth=2.0)
f5_plot.plot(nn, f5_data, linewidth=2.0)
plt.show()

# F1
# 2000 0.000222900000000692 8972633.46789498
# 4000 0.0005126000000004183 7803355.442834054
# 8000 0.0011085999999984608 7216308.858029142
# 16000 0.0019446999999992443 8227490.101304169
# 32000 0.005548799999999687 5767012.687428237
# F2
# 2000 0.5845146999999997 6843283.8387127
# 4000 2.490912399999999 6423349.1310252445
# 8000 7.1087317999999975 9003012.323520213
# 16000 22.427317699999996 11414650.803292453
# 32000 78.08675499999998 13113619.588879064
# F3
# 2000 0.11467160000000831 34882220.18354771
# 4000 0.5041614000000152 31735868.71188377
# 8000 1.9942050999999879 32092987.82758122
# 16000 7.965849399999996 32137188.031699434
# 32000 31.583127400000023 32422374.99254109
# F4
# 2000 0.0015089000000045871 14534805.864707736
# 4000 0.003533399999980702 13545915.305063045
# 8000 0.007510700000011639 13810466.970739873
# 16000 0.0209534999999903 10664211.160650816
# 32000 0.05395860000001562 8875417.396097159
# F5
# 2000 2.4999999936881068e-06 4386313.724939212
# 4000 1.4000000021496817e-06 8546988.761634843
# 8000 1.2999999796647899e-06 9973680.374983825
# 16000 1.4000000021496817e-06 9975560.18801272
# 32000 1.5999999902760464e-06 9353615.23476013
