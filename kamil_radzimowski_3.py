import random
from timeit import default_timer as timer
import sys
import threading
import traceback
import logging

sys.setrecursionlimit(100000000)
threading.stack_size(100000000)


def quickSort(A, p, r):
    if p < r:
        q = partition(A, p, r)
        quickSort(A, p, q)
        quickSort(A, q + 1, r)


def partition(A, p, r):
    x = A[r]
    i = p - 1
    for j in range(p, r + 1):
        if A[j] <= x:
            i = i + 1
            A[i], A[j] = A[j], A[i]
    if i < r:
        return i
    else:
        return i - 1


def sort(Array):
    quickSort(Array, 0, len(Array) - 1)


def customQuickSort(A, p, r, c):
    if p < r:
        # print(f"r - p + 1: {r - p + 1}, c: {c}")
        if r - p + 1 < c:
            bubbleSort(A, p, r)
        else:
            q = partition(A, p, r)
            customQuickSort(A, p, q, c)
            customQuickSort(A, q + 1, r, c)


def bubbleSort(A, p, r):
    wasChanged = True
    while wasChanged:
        wasChanged = False
        for i in range(p, r):
            if A[i] > A[i + 1]:
                wasChanged = True
                A[i], A[i + 1] = A[i + 1], A[i]


def customSort(Array):
    customQuickSort(Array, 0, len(Array) - 1, 7)


def my_case():
    array = [0, 55, 2, 0, 1, 100, 45, 3, 6]
    customSort(array)
    print(array)

    N = [10000]

    print("quickSort")
    for n in N:
        n_arr = []
        for i in range(0, n):
            n_arr.append(random.randint(sys.maxsize * -1, sys.maxsize))
        start = timer()
        sort(n_arr)
        end = timer()
        time = end - start
        print(f"N: {n}, time: {time}")

    print("customQuickSort")
    for n in N:
        n_arr = []
        for i in range(0, n):
            n_arr.append(random.randint(sys.maxsize * -1, sys.maxsize))
        start = timer()
        customSort(n_arr)
        end = timer()
        time = end - start
        print(f"N: {n}, time: {time}")

    print("customQuickSort - pesymistyczne dane")
    for n in N:
        n_arr = []
        for i in range(0, n):
            n_arr.append(i)
        start = timer()
        customSort(n_arr)
        end = timer()
        time = end - start
        print(f"N: {n}, time: {time}")

    print("quickSort - pesymistyczne dane")
    for n in N:
        n_arr = []
        for i in range(0, n):
            n_arr.append(i)
        start = timer()
        sort(n_arr)
        end = timer()
        time = end - start
        print(f"N: {n}, time: {time}")


try:
    #my_case()
    sortowanie = threading.Thread(target=my_case)
    sortowanie.start()
except BaseException as e:
    print(e)
    logging.error(traceback.format_exc())

# my_case()
