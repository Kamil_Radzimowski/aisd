import random


def stringToInt(text):
    result = (ord(text[0]) * 55) + ord(text[1])
    text = text[2:len(text)]
    for char in text:
        result = (result * 55) + ord(char)
    return result


def read_n_lines_from_file(n):
    result = []
    f = open('nazwiskaASCII', "r")
    reader = f.readlines()
    count = 0
    for line in reader:
        if count < n:
            striped = line.split()
            result.append({"int": striped[0], "name": striped[1]})
            count += 1
        else:
            return result
    return result


def ol(arr, item):
    num = stringToInt(item["name"]) % len(arr)
    current = arr[num]
    count = 0
    while current is not None and num + count < len(arr):
        count += 1
        num = (stringToInt(item["name"]) + count) % len(arr)
        current = arr[num]
    if num + count >= len(arr):
        arr.append(item)
    else:
        arr[num] = item
    return count


def ok(arr, item):
    num = stringToInt(item["name"]) % len(arr)
    current = arr[num]
    count = 0
    while current is not None and num + count < len(arr):
        count += 1
        num = (stringToInt(item["name"]) % len(arr) + (count * count)) % len(arr)
        current = arr[num]
    if num + count >= len(arr):
        arr.append(item)
    else:
        arr[num] = item
    return count


def od(arr, item):
    num = stringToInt(item["name"]) % len(arr)
    current = arr[num]
    count = 0
    while current is not None and num + count < len(arr):
        count += 1
        num = ((stringToInt(item["name"]) % len(arr)) + count*(1 + (stringToInt(item["name"]) % (len(arr) - 2)))) % len(arr)
        current = arr[num]
    if num + count >= len(arr):
        arr.append(item)
    else:
        arr[num] = item
    return count


def h(arr, hash_type, percentage):
    hash_result = [None] * len(arr)
    for x in range(len(hash_result)):
        rand = random.randint(0, 100)
        if rand < percentage:
            hash_result[x] = "dummy"
    sum = 0
    for entry in arr:
        sum += hash_type(hash_result, entry)
    avg = sum / len(arr)
    return {"average": avg, "result": hash_result}


res = h(read_n_lines_from_file(5000), ol, 0)
print(res["average"])
