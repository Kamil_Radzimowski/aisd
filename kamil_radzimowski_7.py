import sys


class Node:
    def __init__(self, val):
        self.key = val
        self.left = None
        self.right = None
        self.parent = None


def read_n_lines_from_file(n):
    result = []
    f = open('nazwiskaASCII', "r")
    reader = f.readlines()
    count = 0
    for line in reader:
        if count < n:
            striped = line.split()
            result.append(striped[1])
            count += 1
        else:
            return result
    return result


def insertEntry(current, node):
    if current.key < node:
        if current.right is None:
            current.right = Node(node)
        else:
            insertEntry(current.right, node)
    else:
        if current.left is None:
            current.left = Node(node)
        else:
            insertEntry(current.left, node)


def printInorder(root, counter):
    if root:
        printInorder(root.left, counter + 1)

        print(f"Key: {root.key}, depth: {counter}"),

        printInorder(root.right, counter + 1)


def calculate_height(root, current):
    if root is not None:
        left = calculate_height(root.left, current + 1)
        right = calculate_height(root.right, current + 1)
        return max(left, right)
    else:
        return current


sys.setrecursionlimit(5000)

data = read_n_lines_from_file(5)
root = Node(data[0])
data.pop(0)
for entry in data:
    insertEntry(root, entry)

print(calculate_height(root, 0))
printInorder(root, 1)
