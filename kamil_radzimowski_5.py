file = open("3700.txt", "r")
data = []

for entry in file.readlines():
    data.append(entry)


def stringToInt(text):
    result = (ord(text[0]) * 55) + ord(text[1])
    text = text[2:len(text)]
    for char in text:
        result = (result * 55) + ord(char)
    return result


def weakStringToInt(text):
    return ord(text[0])


def D(m):
    result = [[] for i in range(m)]
    for num in range(2 * m):
        data_row = data[num]
        result[stringToInt(data_row) % m].append(data_row)
    return result


def S(m):
    result = [[] for i in range(m)]
    for num in range(2 * m):
        data_row = data[num]
        result[weakStringToInt(data_row) % m].append(data_row)
    return result


def W(m):
    result = [[] for i in range(m)]
    for num in range(2 * m):
        data_row = data[num]
        result[hash(data_row) % m].append(data_row)
    return result


def numberOfEmptyArrays(current):
    counter = 0
    for array in current:
        if len(array) == 0:
            counter += 1
    return counter


def longestArray(current):
    longest = 0
    for array in current:
        if len(array) > longest:
            longest = len(array)
    return longest


def averageArrayLen(current):
    counter = 0
    container = 0
    for array in current:
        if len(array) != 0:
            counter += 1
            container += len(array)
    return container / counter


arr = [17, 1031, 1024]


def printResult(array, m, typ):
    if m == 17:
        print(array)
    print(f"{typ}{m}, "
          f"emptyArrays: {numberOfEmptyArrays(array)}, "
          f"longestArray: {longestArray(array)}, "
          f"averageLen: {averageArrayLen(array)}")


def test():
    for m in arr:
        d = D(m)
        printResult(d, m, "D")

        s = S(m)
        printResult(s, m, "S")

        w = W(m)
        printResult(w, m, "W")


test()
