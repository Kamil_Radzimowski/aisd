def heapify(arr, current_range, current_index):
    largest_index = current_index
    left_index = (current_index * 2) + 1
    right_index = (current_index * 2) + 2

    if left_index < current_range and arr[left_index] > arr[current_index]:
        largest_index = left_index

    if right_index < current_range and arr[right_index] > arr[largest_index]:
        largest_index = right_index

    if largest_index != current_index:
        arr[current_index], arr[largest_index] = arr[largest_index], arr[current_index]
        heapify(arr, current_range, largest_index)


def heapify_iterative(arr, current_range, starting_index):
    current_index = starting_index
    wasChanged = True
    while wasChanged:
        wasChanged = False
        largest_index = current_index
        left = (current_index * 2) + 1
        right = (current_index * 2) + 2

        if left < current_range and arr[left] > arr[current_index]:
            largest_index = left

        if right < current_range and arr[right] > arr[largest_index]:
            largest_index = right

        if largest_index != current_index:
            arr[current_index], arr[largest_index] = arr[largest_index], arr[current_index]
            wasChanged = True
            current_index = largest_index


def buildMaxHeap(arr):
    last_parent_index = (len(arr) // 2) - 1

    for i in range(last_parent_index, -1, -1):
        heapify_iterative(arr, len(arr), i)
        # heapify(arr, len(arr), i)


def heapSort():
    file1 = open("heap_sort_data.txt", "r")
    arr = []
    for num in file1.readlines():
        arr.append(int(num))

    buildMaxHeap(arr)

    current_len = len(arr)
    for i in range(len(arr) - 1, 0, -1):
        arr[0], arr[current_len - 1] = arr[current_len - 1], arr[0]
        current_len = current_len - 1
        heapify_iterative(arr, current_len, 0)
        # heapify(arr, current_len, 0)

    file2 = open("heap_sort_result.txt", "w")
    for num in arr:
        file2.write(f"{num}\n")


heapSort()
